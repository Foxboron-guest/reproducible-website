---
layout: new/default
title: Help the Reproducible Builds effort!
permalink: /donate/
---

# Help the Reproducible Builds effort!

*The not-for-profit Reproducible Builds effort needs your help to continue its work towards ensuring the security of computer systems of all shapes and sizes around the world.*

We use any and all donated funds to ensure focused and intense work on
ensuring this mission.

The Reproducible Builds Project is affiliated with the [Software Freedom
Conservancy](https://sfconservancy.org/about/) which holds our assets and deal
with financial matters such as donations.

## Paypal

The easiest way to donate to the project is through
[PayPal](https://www.paypal.com). You can use this button to donate to us:

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
  <input type="hidden" name="cmd" value="_s-xclick">
  <input type="hidden" name="hosted_button_id" value="9QA63APRU4TNE">
  <input type="image" src="{{ "/images/paypal_donate.gif" | prepend: site.baseurl }}" border="0" name="submit" alt="Donate to Reproducible Builds via PayPal">
</form>

## Other methods

We can accept check donations drawn in USD from banks in the USA. Donations
from banks outside of the US or not in USD should be handled by wire transfer.

Please make your check payable to "Software Freedom Conservancy, Inc." and to
place "Directed donation: Reproducible Builds" in the memo field. Checks
should then be mailed to:

    Software Freedom Conservancy, Inc.<br>
    137 Montague ST STE 380<br>
    BROOKLYN, NY 11201<br>
    USA

Conservancy also accepts <a href="https://sfconservancy.org/donate/">other
methods to receive donations</a>, including US cheques and wire transfers.
If you are interested please get in touch with us!

## Contact

Please do not hesitate to contact us
([info@reproducible-builds.org](mailto:info@reproducible-builds.org)) for more
information. Thank you for your consideration and we thank you in advance for
your support.
